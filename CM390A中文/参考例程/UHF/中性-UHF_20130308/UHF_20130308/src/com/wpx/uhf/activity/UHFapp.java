package com.wpx.uhf.activity;

import android.app.TabActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

import com.wpx.service.IUHFService;
import com.wpx.service.impl.UHFServiceImpl;

public class UHFapp extends TabActivity {

	private IUHFService uhfService = new UHFServiceImpl();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.uhfapp);
		
		String dev_type = getSharedPreference();
		Log.e("wpx---", "wpx---uhfapp="+dev_type);
        if("".equals(dev_type)){
        	Intent intent = new Intent(this,ChooseDevTypeActivity.class);
        	startActivity(intent);
        }else{
        	setDevType(dev_type);
        }
        
		TabHost tabHost = getTabHost();
		// Scan
        TabSpec scanSpec = tabHost.newTabSpec("Scan");
        scanSpec.setIndicator("Scan");
        Intent IntentScan = new Intent(this, scan.class);
        scanSpec.setContent(IntentScan);

        // access
        TabSpec accessSpec = tabHost.newTabSpec("Access");
        accessSpec.setIndicator("Access");
        Intent IntentAccess = new Intent(this, access.class);
        accessSpec.setContent(IntentAccess);

        // parameters
        TabSpec parametersSpec = tabHost.newTabSpec("Settings");
        parametersSpec.setIndicator("Settings");
        Intent IntentParameters = new Intent(this, parameters.class);
        parametersSpec.setContent(IntentParameters);

		// FW upgrade
        TabSpec fwUpgradeSpec = tabHost.newTabSpec("FW Upgrade");
        fwUpgradeSpec.setIndicator("FW Upgrade");
        Intent IntentFWUpgrade = new Intent(this, fwupgrade.class);
        fwUpgradeSpec.setContent(IntentFWUpgrade);
        
     // test
       // TabSpec test = tabHost.newTabSpec("test");
       // test.setIndicator("test");
        //Intent testintent = new Intent(this, Cilico_UHF_DemoActivity.class);
        //test.setContent(testintent);

        tabHost.addTab(scanSpec);
        tabHost.addTab(accessSpec);
        tabHost.addTab(parametersSpec);
        tabHost.addTab(fwUpgradeSpec);
      //  tabHost.addTab(test);
	}
	
	private void setDevType(String dev_type) {
		// TODO Auto-generated method stub
		int type = -1;
		if("CM390".equals(dev_type)){
			type = 0;
		}else if("CM709".equals(dev_type)){
			type = 1;
		}else if("CM719".equals(dev_type)){
			type = 2;
		}else if("CM719_V1.0".equals(dev_type)){
			type = 3;
		}
		uhfService.setDeviceType(type);
	}
    public boolean onCreateOptionsMenu(Menu menu) {
    	// TODO Auto-generated method stub
    	menu.add(0, 1, 0, "�豸�л�");
    	return super.onCreateOptionsMenu(menu);
    }
    
    public boolean onOptionsItemSelected(MenuItem item) {
    	// TODO Auto-generated method stub
    	switch (item.getItemId()) {
		case 1:
			Intent intent = new Intent(getApplicationContext(),ChooseDevTypeActivity.class);
			startActivity(intent);
			break;

		default:
			break;
		}
    	return super.onOptionsItemSelected(item);
    }

	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		uhfService.disConnected();
	}
	
	
	private String getSharedPreference() {
		// TODO Auto-generated method stub
		SharedPreferences sp = getSharedPreferences("device_type", MODE_PRIVATE);
		String dev_type = sp.getString("device_type_key", "");
		return dev_type;
	}
}
