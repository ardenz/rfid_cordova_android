package com.wpx.uhf.activity;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.wpx.bean.ResultInfo;
import com.wpx.service.IUHFService;
import com.wpx.service.impl.UHFServiceImpl;

public class fwupgrade extends Activity {

	private Button btn_fwupgrade;
	private TextView textView_Status;
	
	private IUHFService uhfService = new UHFServiceImpl();
	//@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fwupgrade);
		setContentView(R.layout.fwupgrade);
		
		init();
	}
	private void init() {
		// TODO Auto-generated method stub
		btn_fwupgrade = (Button)findViewById(R.id.btn_fwupgrade);
		textView_Status = (TextView)findViewById(R.id.textView_Status);
		
		btn_fwupgrade.setOnClickListener(fwUpgradeListener);
	}
	
	private Button.OnClickListener fwUpgradeListener = new Button.OnClickListener(){

		//@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			new threadFWupgrade().execute();
		}
	};
	
	class threadFWupgrade extends AsyncTask<Integer,String,Integer> {
		ResultInfo resultInfo = null;
		//@Override
		protected Integer doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			resultInfo = uhfService.fw_Upgrage();
			return 0;
		}
		
		//@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			textView_Status.setText("Firmware is upgrading...");
			btn_fwupgrade.setEnabled(false);
		}
		
		//@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(resultInfo != null){
				if(resultInfo.getResult() == 0){
					textView_Status.setText("FW upgrade is success.");
				}else {
					textView_Status.setText("FW upgrade is failed " + resultInfo.getResult());
				}
			}
			btn_fwupgrade.setEnabled(true);
		}
	}
}
