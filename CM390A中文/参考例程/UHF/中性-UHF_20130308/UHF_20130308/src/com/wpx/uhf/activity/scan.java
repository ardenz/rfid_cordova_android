package com.wpx.uhf.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.wpx.bean.ResultInfo;
import com.wpx.myutil.PlaySoundUtil;
import com.wpx.service.IUHFService;
import com.wpx.service.impl.UHFServiceImpl;
import com.wpx.util.GlobalUtil;

public class scan extends Activity {

	private Button btn_connect;
	private TextView textView_Version;
	private Button btn_scan;
	private Button btn_ContinusScan;
	private Button btn_StopContinusScan;
	private TextView textView_Status;
	private GridView gridView_EPCs;
	
	private IUHFService uhfService = new UHFServiceImpl();
	int scan_times = 0;
	int find_times =0;
	private MyHandler mHandler;
	//@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scan);
		
		init();
	}
	private void init() {
		// TODO Auto-generated method stub
		btn_connect = (Button)findViewById(R.id.btn_connect);
		textView_Version = (TextView)findViewById(R.id.textView_Version);
		btn_scan = (Button)findViewById(R.id.btn_scan);
		btn_ContinusScan = (Button)findViewById(R.id.btn_ContinusScan);
		btn_StopContinusScan = (Button)findViewById(R.id.btn_StopContinusScan);
		textView_Status = (TextView)findViewById(R.id.textView_Status);
		gridView_EPCs = (GridView)findViewById(R.id.gridView_EPCs);
		btn_scan.setEnabled(false);
		btn_ContinusScan.setEnabled(false);
		
		btn_connect.setOnClickListener(keyConnect);
		btn_scan.setOnClickListener(keyScan);
		btn_ContinusScan.setOnClickListener(keyContinuScan);
		btn_StopContinusScan.setOnClickListener(keyStopContinuScan);
		
		gridView_EPCs.setOnItemClickListener(new OnItemClickListener() {

			//@Override
			public void onItemClick(AdapterView<?> adapter, View view, int position,
					long id) {
				// TODO Auto-generated method stub
				String select_epc = ((TextView)view).getText().toString();
				if(select_epc !=null && !"".equals(select_epc)){
					uhfService.saveSelectedEPC(select_epc);
					Toast.makeText(getApplicationContext(), select_epc, Toast.LENGTH_SHORT).show();
				}else{
					Toast.makeText(getApplicationContext(), "Please select epc first.", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	private Button.OnClickListener keyConnect = new Button.OnClickListener() {
		public void onClick(View v){
			new threadConnect().execute();
		}
	};
	
	private Button.OnClickListener keyScan = new Button.OnClickListener() {
		public void onClick(View v){
			new threadScan().execute();
		}
	};
	
	private Button.OnClickListener keyContinuScan = new Button.OnClickListener() {
		public void onClick(View v){
			mHandler = new MyHandler();
			btn_ContinusScan.setVisibility(View.GONE);
			btn_StopContinusScan.setVisibility(View.VISIBLE);
			scan_times = 0;
			find_times = 0;
			new threadContinuScan().execute();
		}
	};
	
	private Button.OnClickListener keyStopContinuScan = new Button.OnClickListener() {
		public void onClick(View v){
			btn_ContinusScan.setVisibility(View.VISIBLE);
			btn_StopContinusScan.setVisibility(View.GONE);
			mHandler = null;
//			Message msg = new Message();
//			msg.what = 1;
//			mHandler.sendMessage(msg);
		}
	};
	
	class threadConnect extends AsyncTask<Integer,String,Integer> {
		ResultInfo resultInfo = null;
		String version;
		//@Override
		protected Integer doInBackground(Integer... params) {
			 ////TODO Auto-generated method stub
			if(resultInfo !=null){
				if(resultInfo.getResult() == 0){
					version = uhfService.getVersion();
					//uhfService.Channel_Calibration(4+1); //china
				}
			}
			return 0;
		}
		//@Override
		protected void onPreExecute() {
			if("Connect".equals(btn_connect.getText()) || "Open".equals(btn_connect.getText()))
			{
					int type = GlobalUtil.dev_type;
					uhfService.disConnected();
					if(type == 1)
						resultInfo = uhfService.connected(1);
					else{
						resultInfo = uhfService.connected(0);
					}
			}
			if("Close".equals(btn_connect.getText()))
			{
					uhfService.disConnected();
			}			

		}
		//@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			
			if(resultInfo !=null){
				if(resultInfo.getResult() == 0){
					textView_Status.setText("Open port/Get Version.");
					textView_Version.setText(version);
					btn_connect.setText("Close");
					btn_scan.setEnabled(true);
					btn_ContinusScan.setEnabled(true);
				}else {
					btn_connect.setText("Open");
					btn_scan.setEnabled(false);
					btn_ContinusScan.setEnabled(false);
					textView_Status.setText("Open port is failed : "+resultInfo.getResult());
					//clear list
					showEPCList(new ArrayList<String>());
				}
			}else{
				textView_Status.setText("Close Port.");
				textView_Version.setText("");
				btn_connect.setText("Open");
				btn_scan.setEnabled(false);
				btn_ContinusScan.setEnabled(false);
				//clear list
				showEPCList(new ArrayList<String>());
			}
		}
	}
	class threadScan extends AsyncTask<Integer,String,Integer> {
		ResultInfo resultInfo = null;
		//@Override
		protected Integer doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			resultInfo = uhfService.getEPCList(uhfService.getQValue());
			return 0;
		}
		//@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			textView_Status.setText("Scaning.....");
		}
		//@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			if(resultInfo!=null){
				if(resultInfo.getResult() == 0){
					showEPCList(resultInfo.getValues());
					PlaySoundUtil.play();
					textView_Status.setText("Tag is found "+resultInfo.getValues().size());
				}else{
					textView_Status.setText("Search tag is failed : "+resultInfo.getResult());
					//clear list
					showEPCList(new ArrayList<String>());
				}
			}
		}
	}
	
	class threadContinuScan extends AsyncTask<Integer,String,Integer>{
		ResultInfo resultInfo = null;
		//@Override
		protected Integer doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			resultInfo = uhfService.getEPCList(uhfService.getQValue());
			scan_times++;
			return 0;
		}
		
		//@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}
		
		//@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(resultInfo!=null){
				if(resultInfo.getResult() == 0){
					
					showEPCList(resultInfo.getValues());
					PlaySoundUtil.play();
					find_times++;
					textView_Status.setText("Tag is found "+resultInfo.getValues().size()+ ":success"+find_times+ ":scan"+scan_times);
				}else{
					textView_Status.setText("Search tag is failed : "+resultInfo.getResult());
					//clear list
					showEPCList(new ArrayList<String>());
				}
			}else{
				textView_Status.setText("Continus scan is stop.");
			}
			
			Message msg = new Message();
			msg.what = 0;
			if(mHandler != null){
				mHandler.sendMessage(msg);
			}
		}
	}
	
	class MyHandler extends Handler{
		//@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			super.handleMessage(msg);
			super.handleMessage(msg);
			switch (msg.what) {
			case 0:
				new threadContinuScan().execute();
				break;
			case 1:
				break;

			default:
				break;
			}
		}
	}

	public void showEPCList(List<String> values) {
		// TODO Auto-generated method stub
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(scan.this, android.R.layout.simple_list_item_1, values);
		gridView_EPCs.setAdapter(adapter);
	}
}
