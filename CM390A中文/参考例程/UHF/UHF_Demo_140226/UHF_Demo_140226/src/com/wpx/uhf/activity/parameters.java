package com.wpx.uhf.activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.cilico.UHFlib.manager;
import com.wpx.bean.ResultInfo;
import com.wpx.service.IUHFService;
import com.wpx.service.impl.UHFServiceImpl;
import com.wpx.util.GlobalUtil;

public class parameters extends Activity{

	private static final String TAG = "parameters";
	private Spinner spinner_tagType;
	private Spinner spinner_Q;
	private Spinner spinner_Hopping;
	private Spinner spinner_Frequency;
	private Spinner spinner_BLF;
	private Spinner spinner_Modulation;
	private Spinner spinner_PowerLevel;
	
	private boolean		isSpinnerHoppingReady = false;
	private boolean		isSpinnerFrequencyReady = false;
	private boolean		isSpinnerBLFReady = false;
	private boolean		isSpinnerModulationReady = false;
	private boolean		isSpinnerPowerLevelReady = false;
	
	private static int channel_Type = GlobalUtil.gChannelType;
	private static int channel_ID = GlobalUtil.gFrequencyIndex;
	private IUHFService uhfService = new UHFServiceImpl();
	
	//@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.parameters);
		
		init();
	}

	private void init() {
		// TODO Auto-generated method stub
		spinner_tagType = (Spinner)findViewById(R.id.spinner_tagType);
		spinner_tagType.setOnItemSelectedListener(tagtype_listener);
		initSpinner(GlobalUtil.op.tagType, spinner_tagType);
		spinner_tagType.setSelection(GlobalUtil.op.Tag_Type);
		
		spinner_Q = (Spinner)findViewById(R.id.spinner_Q);
		spinner_Q.setOnItemSelectedListener(q_listener);
		
		String[] arrayQ = new String[16];
		for (int i=0; i<16; i++){
			arrayQ[i] = Integer.toString(i);
		}	
		initSpinner(arrayQ, spinner_Q);
		
		spinner_Hopping = (Spinner)findViewById(R.id.spinner_Hopping);
		spinner_Hopping.setOnItemSelectedListener(hopping_listener);
		initSpinner(GlobalUtil.op.tableHopping,spinner_Hopping);
		
		spinner_Frequency = (Spinner)findViewById(R.id.spinner_Frequency);
		spinner_Frequency.setOnItemSelectedListener(frequency_listener);
		initSpinner(GlobalUtil.op.tableFreq, spinner_Frequency);
		
		spinner_BLF = (Spinner)findViewById(R.id.spinner_BLF);
		spinner_BLF.setOnItemSelectedListener(BLF_listener);
		initSpinner(GlobalUtil.op.tableBLFFM0, spinner_BLF);
		spinner_BLF.setSelection(GlobalUtil.op.tableBLFFM0.length - 1);
		
		spinner_Modulation = (Spinner)findViewById(R.id.spinner_Modulation);
		spinner_Modulation.setOnItemSelectedListener(modulation_listener);
		initSpinner(GlobalUtil.op.tableModulation, spinner_Modulation);
		
		spinner_PowerLevel = (Spinner)findViewById(R.id.spinner_PowerLevel);
		initSpinner(GlobalUtil.op.tablePowerLevel, spinner_PowerLevel);
		spinner_PowerLevel.setOnItemSelectedListener(powerLevel_listener);
	}

	private void initSpinner(String[] values,Spinner spinner) {
		// TODO Auto-generated method stub
		ArrayAdapter<String> adapter = new ArrayAdapter<String>
			(this, android.R.layout.simple_spinner_item, values);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
	}
	
	Spinner.OnItemSelectedListener tagtype_listener = new Spinner.OnItemSelectedListener(){

		//@Override
		public void onItemSelected(AdapterView<?> adapter, View view, int position,
				long id) {
			// TODO Auto-generated method stub
			uhfService.setTagType(adapter.getSelectedItemPosition());
		}
		//@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
		}
	};
	
	Spinner.OnItemSelectedListener q_listener = new Spinner.OnItemSelectedListener(){

		//@Override
		public void onItemSelected(AdapterView<?> adapter, View view, int position,
				long id) {
			// TODO Auto-generated method stub
			String str = new String("Q : ");
			str += adapter.getSelectedItem().toString();
			Log.e(TAG, str);
			uhfService.setQValue((int)adapter.getSelectedItemId());
		}
		//@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
		}
	};
	
	Spinner.OnItemSelectedListener hopping_listener = new Spinner.OnItemSelectedListener(){

		//@Override
		public void onItemSelected(AdapterView<?> adapter, View view, int position,
				long id) {
			// TODO Auto-generated method stub
			channel_Type = (int)adapter.getSelectedItemId();
			if (channel_Type == manager.Channel_Type_e_.CH_TYPE_HOPPING){
				spinner_Frequency.setEnabled(false);
			}else{
				spinner_Frequency.setEnabled(true);
			}
			if(!isSpinnerHoppingReady){
				isSpinnerHoppingReady = true;
			}else{
				
				ResultInfo resultInfo = uhfService.set_Channel_Type(channel_Type,channel_ID);
			}
		}
		//@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
		}
	};
	
	Spinner.OnItemSelectedListener frequency_listener = new Spinner.OnItemSelectedListener(){

		//@Override
		public void onItemSelected(AdapterView<?> adapter, View view, int position,
				long id) {
			if(!isSpinnerFrequencyReady){
				isSpinnerFrequencyReady = true;
			}else{
				// TODO Auto-generated method stub
				channel_ID = (int)adapter.getSelectedItemId();
				ResultInfo resultInfo = uhfService.set_Channel_Type(channel_Type,channel_ID);
			}
		}
		//@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
		}
	};
	
	Spinner.OnItemSelectedListener BLF_listener = new Spinner.OnItemSelectedListener(){

		//@Override
		public void onItemSelected(AdapterView<?> adapter, View view, int position,
				long id) {
			// TODO Auto-generated method stub
			if(!isSpinnerBLFReady){
				isSpinnerBLFReady = true;
			}else{
				uhfService.setBLF((int)adapter.getSelectedItemId());
			}
		}
		//@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
		}
	};
	
	Spinner.OnItemSelectedListener modulation_listener = new Spinner.OnItemSelectedListener(){

		//@Override
		public void onItemSelected(AdapterView<?> adapter, View view, int position,
				long id) {
			// TODO Auto-generated method stub
			if(!isSpinnerModulationReady){
				isSpinnerModulationReady = true;
			}else{
				String[] arrayBLF = uhfService.setModulation((int)adapter.getSelectedItemId());
				initSpinner(arrayBLF, spinner_BLF);
				spinner_BLF.setSelection(GlobalUtil.gBLF);
			}
		}
		//@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
		}
	};
	
	Spinner.OnItemSelectedListener powerLevel_listener = new Spinner.OnItemSelectedListener(){

		//@Override
		public void onItemSelected(AdapterView<?> adapter, View view, int position,
				long id) {
			// TODO Auto-generated method stub
			if(!isSpinnerPowerLevelReady){
				isSpinnerPowerLevelReady = true;
			}else{
				uhfService.setPA_Output(adapter.getSelectedItemPosition());
			}
		}
		//@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
		}
	};

}
