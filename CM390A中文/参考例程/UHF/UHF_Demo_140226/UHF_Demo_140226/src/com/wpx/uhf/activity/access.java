package com.wpx.uhf.activity;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.wpx.bean.ResultInfo;
import com.wpx.service.IUHFService;
import com.wpx.service.impl.UHFServiceImpl;

public class access extends Activity {

	private TextView textView_EPC;
	private EditText editText_BlockNum;
	private EditText editText_BlockData;
	private EditText Password;
	private Button btn_blockRead;
	private Button btn_blockWrite;
	private Button ReadReserve;
	private Button WriteReserve;
	private TextView textView_Status;
	private Button readTID;
	private EditText TID;
	private Button ChgEPC;
	private EditText New_EPC;
	private Button WriteKill;
	private EditText KillPassword;
	private Button WriteAccess;
	private EditText AccessPassword;
	private Button LockTag;
	private EditText LockCmd;
	
	private IUHFService uhfService = new UHFServiceImpl();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.access);
		init();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		textView_EPC.setText(uhfService.getSelectedEPC());
		New_EPC.setText(uhfService.getSelectedEPC());
	}
	
	private void init() {
		// TODO Auto-generated method stub
		textView_EPC = (TextView)findViewById(R.id.textView_EPC);
		editText_BlockNum = (EditText)findViewById(R.id.editText_BlockNum);
		editText_BlockData = (EditText)findViewById(R.id.editText_BlockData);
		Password = (EditText)findViewById(R.id.Password);
		btn_blockRead = (Button)findViewById(R.id.btn_blockRead);
		btn_blockWrite = (Button)findViewById(R.id.btn_blockWrite);
		ReadReserve = (Button)findViewById(R.id.ReadReserve);
		WriteReserve = (Button)findViewById(R.id.WriteReserve);
		textView_Status = (TextView)findViewById(R.id.textView_Status);
		readTID = (Button)findViewById(R.id.readTID);
		TID = (EditText)findViewById(R.id.TID);
		ChgEPC = (Button)findViewById(R.id.ChgEPC);
		New_EPC = (EditText)findViewById(R.id.New_EPC);
		WriteKill = (Button)findViewById(R.id.WriteKill);
		KillPassword = (EditText)findViewById(R.id.KillPassword);
		WriteAccess = (Button)findViewById(R.id.WriteAccess);
		AccessPassword = (EditText)findViewById(R.id.AccessPassword);
		LockTag = (Button)findViewById(R.id.LockTag);
		LockCmd = (EditText)findViewById(R.id.LockCmd);
		
		//--------------------------------------------------
		btn_blockRead.setOnClickListener(keyRead);
		btn_blockWrite.setOnClickListener(keyWrite);
		ReadReserve.setOnClickListener(keyReadReserve);
		WriteReserve.setOnClickListener(keyWriteReserve);
		readTID.setOnClickListener(keyReadTID);
		ChgEPC.setOnClickListener(keyChangeEPC);
		WriteKill.setOnClickListener(keyResetPassword);
		WriteAccess.setOnClickListener(keyWritePassword);
		LockTag.setOnClickListener(keyLockTag);
		
		textView_EPC.setText(uhfService.getSelectedEPC());
		New_EPC.setText(uhfService.getSelectedEPC());
		KillPassword.setText("00000000");
		AccessPassword.setText("22222222");
		LockCmd.setText("2222222222");
		Password.setText("00000000");
	}
	
	private Button.OnClickListener keyRead = new Button.OnClickListener() {
		public void onClick(View v){
			new threadRead().execute();
		}
	};
	
	private Button.OnClickListener keyWrite = new Button.OnClickListener() {
		public void onClick(View v){
			new threadWrite().execute();
		}
	};
	
	private Button.OnClickListener keyReadReserve = new Button.OnClickListener() {
		public void onClick(View v){
			new threadReadReserve().execute();
		}
	};
	
	private Button.OnClickListener keyWriteReserve = new Button.OnClickListener() {
		public void onClick(View v){
			new threadWriteReserve().execute();
		}
	};
	
	private Button.OnClickListener keyReadTID = new Button.OnClickListener() {
		public void onClick(View v){
			new threadReadTID().execute();
		}
	};
	
	private Button.OnClickListener keyChangeEPC = new Button.OnClickListener() {
		public void onClick(View v){
			new threadChangeEPC().execute();
		}
	};
	
	private Button.OnClickListener keyResetPassword = new Button.OnClickListener(){
		public void onClick(View v){
			new threadResetPassword().execute();
		}	
	};
	
	private Button.OnClickListener keyWritePassword = new Button.OnClickListener(){
		public void onClick(View v){
			new threadWritePassword().execute();
		}	
	};	
	
	private Button.OnClickListener keyLockTag = new Button.OnClickListener(){
		public void onClick(View v){
			new threadLockTag().execute();
		}	
	};	
	
	class threadRead extends AsyncTask<Integer,String,Integer> {
		ResultInfo resultInfo = null;
		@Override
		protected Integer doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			String id = editText_BlockNum.getText().toString();
			String password = Password.getText().toString();
			int block_Id = -1;
			byte read_len = 1;
			if(id != null && !"".equals(id)){
				block_Id = Integer.parseInt(id);
			}
			resultInfo = uhfService.readUser(block_Id, password,read_len);
			//resultInfo = uhfService.readBank1(block_Id, password,read_len);
			return 0;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			textView_Status.setText("Read user data ....");
		}
		
		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(resultInfo != null){
				if("".equals(resultInfo.getErrInfo())){
					if(resultInfo.getResult() == 0){
						editText_BlockData.setText(resultInfo.getResultValue());
						textView_Status.setText("Read user data success.");
					}else {
						textView_Status.setText("Read user data faild : " + resultInfo.getResult());
					}
				}else{
					textView_Status.setText(resultInfo.getErrInfo());
				}
			}
		}
		
	}
	class threadWrite extends AsyncTask<Integer,String,Integer> {
		ResultInfo resultInfo = null;
		@Override
		protected Integer doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			String id = editText_BlockNum.getText().toString();
			String password = Password.getText().toString();
			int block_Id = -1;
			if(id != null && !"".equals(id)){
				block_Id = Integer.parseInt(id);
			}
			String block_Data = editText_BlockData.getText().toString();
			resultInfo = uhfService.writeUser(block_Id, password, block_Data);
			//resultInfo = uhfService.writeBank1(block_Id, password, block_Data);
			return 0;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			textView_Status.setText("Write user data ....");
		}
		
		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(resultInfo !=null){
				if("".equals(resultInfo.getErrInfo())){
					if(resultInfo.getResult() == 0){
						textView_Status.setText("Write user data success.");
					}else {
						textView_Status.setText("Write user data faild : " +resultInfo.getResult());
					}
				}else{
					textView_Status.setText(resultInfo.getErrInfo());
				}
			}
		}
	}
	
	class threadReadReserve extends AsyncTask<Integer,String,Integer> {
		ResultInfo resultInfo = null;
		@Override
		protected Integer doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			String id = editText_BlockNum.getText().toString();
			String password = Password.getText().toString();
			int block_Id = -1;
			if(id != null && !"".equals(id)){
				block_Id = Integer.parseInt(id);
			}
			resultInfo = uhfService.readReserve(block_Id, password);
			return 0;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			textView_Status.setText("Read reserve data ....");
		}
		
		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(resultInfo !=null ){
				if("".equals(resultInfo.getErrInfo())){
					if(resultInfo.getResult() == 0){
						editText_BlockData.setText(resultInfo.getResultValue());
						textView_Status.setText("Read reserve data success.");
					}else{
						textView_Status.setText("Read reserve data faild " + resultInfo.getResult());
					}
				}else{
					textView_Status.setText(resultInfo.getErrInfo());
				}
			}
		}
	}
	
	class threadWriteReserve extends AsyncTask<Integer,String,Integer> {
		ResultInfo resultInfo = null;
		@Override
		protected Integer doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			String id = editText_BlockNum.getText().toString();
			String password = Password.getText().toString();
			int block_Id = -1;
			if(id != null && !"".equals(id)){
				block_Id = Integer.parseInt(id);
			}
			String block_Data = editText_BlockData.getText().toString();
			resultInfo = uhfService.writeReserve(block_Id, password, block_Data);
			return 0;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			textView_Status.setText("Write reserve data ....");
		}
		
		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(resultInfo !=null){
				if("".equals(resultInfo.getErrInfo())){
					if(resultInfo.getResult() == 0){
						textView_Status.setText("Write reserve data success.");
					}else{
						textView_Status.setText("Write reserve data faild " + resultInfo.getResult());
					}
				}else{
					textView_Status.setText(resultInfo.getErrInfo());
				}
			}
		}
	}
	
	class threadReadTID extends AsyncTask<Integer,String,Integer> {
		ResultInfo resultInfo = null;
		@Override
		protected Integer doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			String password = Password.getText().toString();
			resultInfo = uhfService.readTID(password);
			return 0;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			textView_Status.setText("Read TID...");
		}
		
		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(resultInfo !=null ){
				if("".equals(resultInfo.getErrInfo())){
					if(resultInfo.getResult() == 0){
						TID.setText(resultInfo.getResultValue());
						textView_Status.setText("Read TID data success.");
					}else{
						textView_Status.setText("Read TID data faild "+resultInfo.getResult());
					}
				}else{
					textView_Status.setText(resultInfo.getErrInfo());
				}
			}
		}
		
	}
	
	class threadChangeEPC extends AsyncTask<Integer,String,Integer> {
		ResultInfo resultInfo = null;
		@Override
		protected Integer doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			String password = Password.getText().toString();
			String new_epc = New_EPC.getText().toString();
			resultInfo = uhfService.changeEPC(new_epc, password);
			return 0;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			textView_Status.setText("change epc data ....");
		}
		
		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(resultInfo !=null){
				if("".equals(resultInfo.getErrInfo())){
					if(resultInfo.getResult() == 0){
						textView_Status.setText("Chang EPC success.");
					}else{
						textView_Status.setText("Chang EPC faild "+resultInfo.getResult());
					}
				}else{
					textView_Status.setText(resultInfo.getErrInfo());
				}
			}
		}
	}
	
	class threadResetPassword extends AsyncTask<Integer,String,Integer> {
		ResultInfo resultInfo = null;
		@Override
		protected Integer doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			String old_password = Password.getText().toString();
			String new_password = KillPassword.getText().toString();
			
			resultInfo = uhfService.resetPassword(old_password,new_password);
			return 0;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			textView_Status.setText("Reset Passwrod.....");
		}
		
		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(resultInfo !=null){
				if("".equals(resultInfo.getErrInfo())){
					if(resultInfo.getResult() == 0){
						textView_Status.setText("Reset Password Success");
					}else{
						textView_Status.setText("Reset Password Faild"+ resultInfo.getResult());
					}
				}else{
					textView_Status.setText(resultInfo.getErrInfo());
				}
			}
		}
	}
	
	class threadWritePassword extends AsyncTask<Integer,String,Integer> {
		ResultInfo resultInfo = null;
		@Override
		protected Integer doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			String old_Password = Password.getText().toString();
			String new_Password = AccessPassword.getText().toString();
			resultInfo = uhfService.writePassword(old_Password, new_Password);
			return 0;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
		}
		
		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(resultInfo !=null){
				if("".equals(resultInfo.getErrInfo())){
					if(resultInfo.getResult() == 0){
						textView_Status.setText("Write AccessPassword Success");
					}else{
						textView_Status.setText("Reset AccessPassword Faild"+ resultInfo.getResult());
					}
				}else{
					textView_Status.setText(resultInfo.getErrInfo());
				}
			}
		}
		
	}
	
	class threadLockTag extends AsyncTask<Integer,String,Integer> {
		ResultInfo resultInfo = null;
		@Override
		protected Integer doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			String password = Password.getText().toString();
			String lock_cmd = LockCmd.getText().toString();
			resultInfo = uhfService.lockTag(password, lock_cmd);
			return 0;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			textView_Status.setText("lock tag ....");
		}
		
		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(resultInfo !=null){
				if("".equals(resultInfo.getErrInfo())){
					if(resultInfo.getResult() == 0){
						textView_Status.setText("Lock Tag Success");
					}else{
						textView_Status.setText("Lock Tag Faild"+ resultInfo.getResult());
					}
				}else{
					textView_Status.setText(resultInfo.getErrInfo());
				}
			}
		}
	}
}
