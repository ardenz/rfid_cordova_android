package com.wpx.uhf.activity;

import com.wpx.service.IUHFService;
import com.wpx.service.impl.UHFServiceImpl;

import android.app.ListActivity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ChooseDevTypeActivity extends ListActivity {

	private IUHFService uhfService = new UHFServiceImpl();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_LEFT_ICON);

		setListAdapter(initListAdapter());
		final ListView listView = getListView();
		listView.setItemsCanFocus(false);
		listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		listView.setOnItemClickListener(dev_type_select);

		getWindow().setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, 0);

	}
	
	private AdapterView.OnItemClickListener dev_type_select = new AdapterView.OnItemClickListener() {

		public void onItemClick(AdapterView<?> adapter, View view, int position,
				long id) {
			// TODO Auto-generated method stub
			String type = adapter.getItemAtPosition(position).toString();
			SharedPreferences sp = getSharedPreferences("device_type", MODE_PRIVATE);
			SharedPreferences.Editor editor = sp.edit();
			editor.putString("device_type_key", type);
			editor.commit();
			
			setDevType(type);
			Log.e("wpx--", "wpx--dev_type_select=" +type);
			finish();
		}

	}; 

	private ArrayAdapter<String> initListAdapter() {
		return  new ArrayAdapter<String>(
				getApplicationContext(),
				android.R.layout.simple_list_item_single_choice, DEV_TYPS);
	}

	private static final String[] DEV_TYPS = new String[] { "CM390", "CM709",
			"CM719", "CM719_V1.0" };

	private void setDevType(String dev_type) {
		// TODO Auto-generated method stub
		int type = -1;
		if("CM390".equals(dev_type)){
			type = 0;
		}else if("CM709".equals(dev_type)){
			type = 1;
		}else if("CM719".equals(dev_type)){
			type = 2;
		}else if("CM719_V1.0".equals(dev_type)){
			type = 3;
		}
		uhfService.setDeviceType(type);
	}
}
