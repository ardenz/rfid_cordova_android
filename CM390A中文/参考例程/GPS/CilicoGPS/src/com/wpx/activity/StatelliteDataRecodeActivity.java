package com.wpx.activity;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.security.SecureClassLoader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.Service;
import android.content.Intent;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.provider.Settings;
import android.widget.TextView;
import android.widget.Toast;

public class StatelliteDataRecodeActivity extends Activity {
	/** Called when the activity is first created. */

	private TextView longitude;
	private TextView latitude;
	private TextView time;
	private TextView altitude;

	private LocationManager locationManager;
	private LocationListener locaionListener;

	private PowerManager pm;
	private WakeLock wakeLock;

	private String tempTime="";
	
	private Timer timer = new Timer();
	private TimerTask timerTask = new TimerTask() {
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			
		}
	};
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if(!isgpsOpen()){
			toggleGPS();
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		pm = (PowerManager) getSystemService(Service.POWER_SERVICE);
		wakeLock = pm
				.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK
						| PowerManager.ON_AFTER_RELEASE,
						"StatelliteDataRecodeActivity");

		longitude = (TextView) findViewById(R.id.longitude);
		latitude = (TextView) findViewById(R.id.latitude);
		time = (TextView) findViewById(R.id.time);
		altitude = (TextView) findViewById(R.id.altitude);

		locationManager = (LocationManager) this
				.getSystemService(StatelliteDataRecodeActivity.LOCATION_SERVICE);

		locationManager.addGpsStatusListener(statuListener);

		initLocationListener();
		locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER,
				1000, 0, locaionListener);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		wakeLock.acquire();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		wakeLock.acquire();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		wakeLock.release();
		
		if(isgpsOpen()){
			toggleGPS();
		}
	}

	private void toggleGPS() {
		Intent gpsIntent = new Intent();
		gpsIntent.setClassName("com.android.settings",
				"com.android.settings.widget.SettingsAppWidgetProvider");
		gpsIntent.addCategory("android.intent.category.ALTERNATIVE");
		gpsIntent.setData(Uri.parse("custom:3"));
		try {
			PendingIntent.getBroadcast(this, 0, gpsIntent, 0).send();
		} catch (CanceledException e) {
			e.printStackTrace();
		}
	}

	private boolean isgpsOpen() { // ture gps 开启  ,false gps 关闭 
		boolean flag = false;
		String str = Settings.Secure.getString(this.getContentResolver(),
				Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
		if(str !=null ){
			flag = str.contains("gps");
		}

		return flag;
	}

	private void initLocationListener() {
		// TODO Auto-generated method stub
		locaionListener = new LocationListener() {

			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
				// TODO Auto-generated method stub
			}

			// gps 开启时触发
			@Override
			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub
				// Toast.makeText(StatelliteDataRecodeActivity.this,
				// "GPS以正常使用!!!", Toast.LENGTH_SHORT).show();
			}

			// gps禁用时触发
			@Override
			public void onProviderDisabled(String provider) {
				// TODO Auto-generated method stub
				// Toast.makeText(StatelliteDataRecodeActivity.this,
				// "GPS已禁用,请先开启功能!!!", Toast.LENGTH_SHORT).show();
				// Intent intent = new
				// Intent(Settings.ACTION_SECURITY_SETTINGS);
				// startActivityForResult(intent, 0);

			}

			// 位置变化时触发
			@Override
			public void onLocationChanged(Location location) {
				// TODO Auto-generated method stub
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				String locationIime =df.format(new Date(location.getTime())); 
				
				time.setText(locationIime);
				longitude.setText(formateGPSData(location.getLongitude()));
				latitude.setText(formateGPSData(location.getLatitude()));
				altitude.setText("" + location.getAltitude());

				String alt = "" + location.getAltitude();
				
				if (alt.indexOf(".") > 0) {
					alt = alt.substring(0, alt.indexOf(".") + 2);
				}

				String dataall = df.format(new Date(location.getTime())) + ",\t"
						+ location.getLongitude() + ",\t"
						+ location.getLatitude() + ",\t" + location.getAltitude()
						+ "\n\n";
				String data = location.getLongitude() + ","
						+ location.getLatitude() + "," + alt
						+ "\n\n";
				
				if(!tempTime.equals(locationIime)){
					recordGPSData("/data/gpsdata.txt",data);
					recordGPSData("/data/gpsdataall.txt", dataall);
					
					tempTime = locationIime;
				}
			}
		};
	}

	protected String formateGPSData(double data) {
		// TODO Auto-generated method stub
		String str = data+"";
		StringBuffer sb = new StringBuffer();
		String du = str.substring(0, str.indexOf("."));
		sb.append(du+"° ");
		
		String strfen = "0"+str.substring(str.indexOf("."), str.length());
		Double doufen = Double.parseDouble(strfen) * 60;
		String fen = doufen.toString().substring(0, doufen.toString().indexOf("."));
		sb.append(fen+"' ");
		
		String strmiao = "0"+doufen.toString().substring(doufen.toString().indexOf("."), doufen.toString().length());
		Double doumiao = Double.parseDouble(strmiao) * 60;
		DecimalFormat df = new DecimalFormat("##.000000");
		sb.append(df.format(doumiao));
		sb.append("\" ");
		
		return sb.toString();
	}

	protected void recordGPSData(String datapath,String data) {
		// TODO Auto-generated method stub

		File f = new File(datapath);

		byte[] gpsdata = data.getBytes();
		// byte[] gpsdata ="123456789abc".getBytes();
		try {
			RandomAccessFile rf = new RandomAccessFile(datapath,"rw");
			rf.seek(rf.length());// 将指针移动到文件末尾
			rf.write(gpsdata);
			rf.close();

			// FileOutputStream fos = new FileOutputStream(
			// "/data/gpsdata.txt");
			// DataOutputStream out = new DataOutputStream(fos);
			// out.write(gpsdata, 0, gpsdata.length);
			//
			// out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private final GpsStatus.Listener statuListener = new GpsStatus.Listener() {
		@Override
		public void onGpsStatusChanged(int event) {
			// TODO Auto-generated method stub
			GpsStatus statu = locationManager.getGpsStatus(null);
			updateGpsStatu(event, statu);
		}
	};

	protected void updateGpsStatu(int event, GpsStatus statu) {
		// TODO Auto-generated method stub
		int count = 0;
		if (statu == null) {
		} else if (event == GpsStatus.GPS_EVENT_SATELLITE_STATUS) {
		}
	}
	
}