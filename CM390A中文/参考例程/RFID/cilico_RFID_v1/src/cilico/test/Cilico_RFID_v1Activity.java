package cilico.test;

import cilico.tools.I2CTools;
import android.app.Activity;
import android.os.Bundle;
//import android.os.Bundle;
//import android.os.SystemClock;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Cilico_RFID_v1Activity extends Activity {
	/** Called when the activity is first created. */
	String strUI = "";
	TextView resultView_MF1;
	Button xunka, qingping, duqu, xieru, readmore, writemore;
	EditText kuaihao, inputdata, start, end, writemoredata, mima;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		xunka = (Button) findViewById(R.id.xunka);
		mima = (EditText) findViewById(R.id.mima);
		resultView_MF1 = (TextView) findViewById(R.id.resultView_MF1);
		qingping = (Button) findViewById(R.id.qingping);
		kuaihao = (EditText) findViewById(R.id.kuaihao);
		duqu = (Button) findViewById(R.id.duqu);
		xieru = (Button) findViewById(R.id.xieru);
		inputdata = (EditText) findViewById(R.id.inputdata);
		readmore = (Button) findViewById(R.id.readmore);
		start = (EditText) findViewById(R.id.start);
		end = (EditText) findViewById(R.id.end);
		writemore = (Button) findViewById(R.id.writemore);
		writemoredata = (EditText) findViewById(R.id.writemoredata);

		showToast(String.valueOf(I2CTools.readVersion()));
		showToast(I2CTools.ReadUID());

		qingping.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				strUI = "";

				resultView_MF1.setText("");
			}
		});

		xunka.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String t = I2CTools.ReadUID();

				if (t.length() != 0) {
					strUI += ("<UID> : " + t + "\n");
				} else {
					strUI += ("ERROR\n");
				}
				resultView_MF1.setText(strUI);
			}
		});
		
		// read one block
		duqu.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String mimaStr = mima.getText().toString().trim();
				byte[] passw = I2CTools.stringToBytes(mimaStr);
				String address = kuaihao.getText().toString().trim();

				if (address == null || address.equals("")) {
					strUI += "address must be not null!!\n";
				} else {
					byte[] buffer = new byte[16];
					int add = Integer.valueOf(address);
					int t;

					t = I2CTools.ReadBlock(buffer, passw, (byte) 0x60,
							(byte) add);

					if (t == 0) {
						String str = new String(buffer);
						strUI += ("<DATA> " + add + " :\n" + str + "\n");
					} else {
						strUI += ("Read " + add + " :" + "ERROR\n");
					}
				}
				resultView_MF1.setText(strUI);
			}
		});
		
		// write one block
		xieru.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String mimaStr = mima.getText().toString().trim();
				byte[] passw = I2CTools.stringToBytes(mimaStr);
				String address = kuaihao.getText().toString().trim(); // block
																		// number
				String dataE = inputdata.getText().toString().trim();
				byte[] myByte = dataE.getBytes();

				byte[] pwrite = new byte[16];
				System.arraycopy(myByte, 0, pwrite, 0, myByte.length > 16 ? 16
						: myByte.length);

				if (address == null || address.equals("")) {
					strUI += "Address must be not null!!\n";
				} else {
					int add = Integer.valueOf(address);
					int sec = I2CTools.WriteBlock(pwrite, passw, (byte) 0x60,
							(byte) add);

					if (sec != 0) {
						strUI += "write data by ADDRESS: " + add + " failed!\n";
					} else {
						strUI += ("write data by ADDRESS: " + add + " success!\n");
					}
				}

				resultView_MF1.setText(strUI);
			}
		});

		// read more than one block
		readmore.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String mimaStr = mima.getText().toString().trim();
				byte[] passw = I2CTools.stringToBytes(mimaStr);
				String startadd = start.getText().toString().trim();
				String endadd = end.getText().toString().trim();

				if (startadd == null || startadd.equals("") || endadd == null
						|| endadd.equals("")) {
					strUI += "address must be not null!!\n";
				} else {
					byte[] buffer = new byte[32768];
					int[] count = new int[16];

					int add1 = Integer.valueOf(startadd);
					int add2 = Integer.valueOf(endadd);
					int t;

					t = I2CTools.ReadBlocksEx(buffer, count, passw,
							(byte) 0x60, add1, add2);

					if (t == 0) {
						byte[] tempdata = new byte[count[0]];
						for (int i = 0; i < count[0]; i++) {
							tempdata[i] = buffer[i];
						}
						String str = new String(tempdata);
						strUI += ("Data read from more block " + " :\n" + str + "\n");
					} else {
						strUI += ("Read ERROR\n");
					}
				}
				resultView_MF1.setText(strUI);

			}
		});

		writemore.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String mimaStr = mima.getText().toString().trim();
				byte[] passw = I2CTools.stringToBytes(mimaStr);

				String startadd = start.getText().toString().trim();
				String endadd = end.getText().toString().trim();
				String dataE = writemoredata.getText().toString().trim();
				byte[] myByte = dataE.getBytes();

				if (startadd == null || startadd.equals("") || endadd == null
						|| endadd.equals("")) {
					strUI += "address must be not null!!\n";
				} else {

					int[] count = new int[16];

					int add1 = Integer.valueOf(startadd);
					int add2 = Integer.valueOf(endadd);
					int t = 0;

					t = I2CTools.WriteBlocksEx(myByte, count, passw,
							(byte) 0x60, add1, add2);

					if (t == 0) {
						strUI += ("Write more block success" + " \n");
					} else {
						strUI += ("Writemore ERROR\n");
					}
				}
				resultView_MF1.setText(strUI);

			}
		});

	}

	private void showToast(String str) {
		Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
	}

}