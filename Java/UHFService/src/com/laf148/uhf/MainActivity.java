package com.laf148.uhf;


import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
	private Button btnConnect;
	private Button btnDisconnect;
	private Button btnScan;
	private Button btnContScan;
	private Button btnStopContScan;
	private Button btnReadUser;
	private TextView textView1;
	private boolean _bounded = false;
	private Messenger mService = null;


	static final int MSG_CONNECT = 1;
	static final int MSG_DISCONNECT = 2;
	static final int MSG_SCAN = 3;
	static final int MSG_READUSERZONE = 4;
	MyHandler _handler;

	//@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainactivity);
		btnConnect = (Button)this.findViewById(R.id.btnConnect);
		btnDisconnect = (Button)this.findViewById(R.id.btnDisconnect);
		btnScan = (Button)this.findViewById(R.id.btnScan);
		btnContScan = (Button)this.findViewById(R.id.btnContScan);
		btnStopContScan = (Button)this.findViewById(R.id.btnStopContScan);
		btnReadUser = (Button)this.findViewById(R.id.btnReadUser);
		textView1 = (TextView)this.findViewById(R.id.textView1);
		
		btnConnect.setOnClickListener(keyConnect);
		btnDisconnect.setOnClickListener(keyDisconnect);
		btnScan.setOnClickListener(keyScan);
		btnContScan.setOnClickListener(keyContinuScan);
		btnStopContScan.setOnClickListener(keyStopScan);
		btnReadUser.setOnClickListener(keyReadUser);

	}
	
	private Button.OnClickListener keyConnect = new Button.OnClickListener() {
		public void onClick(View v){
			if (_bounded){
				try {
                        Message msg = Message.obtain(null,  MSG_CONNECT);
                        msg.replyTo = mMessenger;
                        mService.send(msg);

                } catch (RemoteException e) {
                	textView1.setText("Fail to connect");
                }

				textView1.setText("Sending");
			}
			else {
				textView1.setText("unbounded");

			}
		}
	};

	private Button.OnClickListener keyDisconnect = new Button.OnClickListener() {
		public void onClick(View v){
			if (_bounded){
				try {
                        Message msg = Message.obtain(null,  MSG_DISCONNECT);
                        msg.replyTo = mMessenger;
                        mService.send(msg);

                } catch (RemoteException e) {
                	textView1.setText("Fail to disconnect");
                }

				textView1.setText("Sending");
			}
			else {
				textView1.setText("unbounded");
			}
		}
	};

	private Button.OnClickListener keyScan = new Button.OnClickListener() {
		public void onClick(View v){
			scan(0);
			textView1.setText("Sending");
		}
	};

	private void scan(int q){
		try {
			_isScaning = true;
            Message msg = Message.obtain(null,  MSG_SCAN);
            msg.replyTo = mMessenger;
            msg.arg1 = q;
            mService.send(msg);

        } catch (RemoteException e) {
        	textView1.setText("Fail to scan");
        }
	}
	
	private Button.OnClickListener keyContinuScan = new Button.OnClickListener() {
		public void onClick(View v){
			_handler = new MyHandler();
			contScan();
		}
	};
	
	private boolean _isScaning = false;
	private void contScan()
	{
		if (_handler!=null){
			if (!_isScaning)
				scan(4);
			
			
			Message msg = new Message();
			msg.what = 0;
			if(_handler != null){
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				_handler.sendMessage(msg);
			}

		}
	}

	private Button.OnClickListener keyStopScan = new Button.OnClickListener() {
		public void onClick(View v){
			_handler = null;
		}
	};
	
	private Button.OnClickListener keyReadUser = new Button.OnClickListener() {
		public void onClick(View v){
			try {
	            Message msg = Message.obtain(null,  MSG_READUSERZONE);
	            msg.arg1 = 0;
	            msg.arg2 = 7;
	            msg.replyTo = mMessenger;
	            mService.send(msg);

	        } catch (RemoteException e) {
	        	textView1.setText("Fail to read user zone");
	        }
		}
	}; 


    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
        	Bundle data;
        	int rs;
            switch (msg.what) {
            case MSG_CONNECT:
            case MSG_DISCONNECT:
                data = msg.getData();
                rs = data.getInt("result");
                if (rs==0) {
                	if (msg.what == MSG_CONNECT)
                		textView1.setText("Connected");
                	else
                		textView1.setText("Disconnected");
                }
                else
                	textView1.setText(Integer.toString(rs));
                break;
            case MSG_SCAN:
            	_isScaning = false;
            	data = msg.getData();
            	rs = data.getInt("result");
            	if  (rs==0){
            		StringBuilder sb = new StringBuilder();
            		for (String s : data.getStringArrayList("tags")){
            			sb.append(s).append("\n");
            		}
            		
            		textView1.setText(sb.toString());
            	}
            	else
            		textView1.setText("Error:" + Integer.toString(rs));
            	break;
            case MSG_READUSERZONE:
            	data = msg.getData();
            	rs = data.getInt("result");
            	if  (rs==0){
            		textView1.setText(data.getString("userzone"));
            	}
            	else
            		textView1.setText("Error:" + Integer.toString(rs));
            	break;
            default:
                    super.handleMessage(msg);
            }
        }
	}
	
	/**
	 * Target we publish for clients to send messages to IncomingHandler.
	 */
	final Messenger mMessenger = new Messenger(new IncomingHandler());

   private ServiceConnection mConnection = new ServiceConnection() {  
 
       @Override  
       public void onServiceConnected(ComponentName className,  
               IBinder service) {  
    	   mService = new Messenger(service);
    	   textView1.setText("Attached.");
           _bounded = true;  
       }  
 
       @Override  
       public void onServiceDisconnected(ComponentName arg0) {  
    	   mService = null;
    	   textView1.setText("Detached.");
    	   _bounded = false;  
       }  
   };  
	
	 @Override  
    protected void onStart() {  
        super.onStart();  
        
        Intent intent = new Intent();
        intent.setAction("laf148.intent.action.UHFSERVICE");
        
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }  

   @Override  
    protected void onStop() {  
        super.onStop();  
        // ��service�����  
        if (_bounded) {  
            unbindService(mConnection);  
            _bounded = false;  
        }  
    }  
	
	class MyHandler extends Handler{
		//@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case 0:
				//new threadContinuScan().execute();
				contScan();
				break;
			case 1:
				break;

			default:
				break;
			}
		}
	}

}
