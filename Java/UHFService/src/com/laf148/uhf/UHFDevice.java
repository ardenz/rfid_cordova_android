package com.laf148.uhf;


import com.wpx.bean.ResultInfo;
import com.wpx.service.IUHFService;
import com.wpx.service.impl.UHFServiceImpl;

public class UHFDevice {
	
	private IUHFService uhfService = new UHFServiceImpl();
	private static ResultInfo _nullResult;
		
	private UHFDevice() {
		uhfService.setDeviceType(0);
//		uhfService.setQValue(2);
		
		_nullResult =  new ResultInfo();
		_nullResult.setResult(-1);
	}
	
	private static UHFDevice _instance;
	public static UHFDevice getInstance(){
		if (_instance == null)
			_instance = new UHFDevice();
		return _instance;
	}
	
	public ResultInfo connect(){
		uhfService.disConnected();
		ResultInfo resultInfo = uhfService.connected(0);
		//uhfService.setQValue(2);
		if (resultInfo == null)
			return _nullResult;
		else
			return resultInfo;
/*		String version = "Can't connect";
		if(resultInfo !=null){
			if(resultInfo.getResult() == 0){
				version = "success";
			}
			else {
				version = "Error:" + Integer.toString(resultInfo.getResult());
			}
		}
		return version;
		*/
	}
	
	public void disconnect(){
		uhfService.disConnected();
	}

	public ResultInfo scan(int q){
		
		int oldQ = uhfService.getQValue();
		if (oldQ!=q)
			uhfService.setQValue(q);
			
		//uhfService.setQValue(2);
		//q = uhfService.getQValue();
		ResultInfo resultInfo = uhfService.getEPCList(q);
		if (resultInfo!=null)
			return resultInfo;
		else
			return _nullResult;
	}
	
	public ResultInfo readUser(int blockId,String password,int readLen){
		if (password == null || password.length()==0)
			password = "00000000";
		
		ResultInfo resultInfo = uhfService.readUser(blockId, password, readLen);
		if (resultInfo!=null)
			return resultInfo;
		else
			return _nullResult;
	}
}
