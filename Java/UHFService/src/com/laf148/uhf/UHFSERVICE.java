package com.laf148.uhf;



import java.util.ArrayList;
import java.util.List;

import com.wpx.bean.ResultInfo;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

public class UHFSERVICE  extends Service  {
	/**
     * Handler of incoming messages from clients.
     */
	static final int MSG_CONNECT = 1;
	static final int MSG_DISCONNECT = 2;
	static final int MSG_SCAN = 3;
	static final int MSG_READUSERZONE = 4;


	static class ScanParam {
		public int q;
		public Messenger replyTo;
		public ScanParam(int q, Messenger replyTo){
			this.q = q;
			this.replyTo = replyTo;
		}
	}
	
	static class ReadUserParam {
		public int blockId;
		public String password;
		public int readLen;
		public Messenger replyTo;
		public ReadUserParam(int blockId, String password, int readLen, Messenger replyTo){
			this.blockId = blockId;
			this.password = password;
			this.readLen = readLen;
			this.replyTo = replyTo;
		}
	}
	
    static class IncomingHandler extends Handler {
            @Override
            public void handleMessage(Message msg) {
                    switch (msg.what) {
	                    case MSG_CONNECT:
	                    	new threadConnect().execute(msg.replyTo);
	                    	/*// it has a exception when getVersion is not in another thread
  	                    	 String version = dev.connect();
	                   		 Bundle data = new Bundle();
	                   		 data.putString("version", version);
	                   		 rMsg.setData(data);
	                    	 try {
	                             msg.replyTo.send(rMsg);
		                     } catch (RemoteException e) {
		                     }*/
		
		                     break;
	                    case MSG_DISCONNECT:
	                    	new threadDisconnect().execute(msg.replyTo);
	                    	break;
	                    case MSG_SCAN:
	                    	new threadScan().execute(new ScanParam(msg.arg1, msg.replyTo)); // msg.replyTo will be set to null when the task run in background
	                    	break;
	                    case MSG_READUSERZONE:
	                    	int blockId = msg.arg1;
	                    	int readLen = msg.arg2;
	                    	String password = msg.getData().getString("password");
	                    	new threadReadUser().execute(new ReadUserParam(blockId, password, readLen, msg.replyTo));
	                    			
	                    	break;
	                    default:
                            super.handleMessage(msg);
                    }
            }
    }
    
	static class threadConnect extends AsyncTask<Messenger,String,ResultInfo> {
		Messenger replyTo;
		@Override
		protected ResultInfo doInBackground(Messenger... params) {
			replyTo = params[0];
			return UHFDevice.getInstance().connect();
		}
		
		@Override
		protected void onPreExecute() {
		}
		
		@Override
		protected void onPostExecute(ResultInfo result) {
			 Message rMsg = Message.obtain(null, MSG_CONNECT);
      		 Bundle data = new Bundle();
      		 data.putInt("result", result.getResult());
      		 rMsg.setData(data);
      		 try {
                replyTo.send(rMsg);
      		 } catch (RemoteException e) {
      		 }

		}
	}

	static class threadDisconnect extends AsyncTask<Messenger,String,Integer> {
		Messenger replyTo;
		@Override
		protected Integer doInBackground(Messenger... params) {
			replyTo = params[0];
			UHFDevice.getInstance().disconnect();
			return 0;
		}
		
		@Override
		protected void onPreExecute() {
		}
		
		@Override
		protected void onPostExecute(Integer result) {
			 Message rMsg = Message.obtain(null, MSG_DISCONNECT);
      		 Bundle data = new Bundle();
      		 data.putInt("result", result);
      		 rMsg.setData(data);
      		 try {
                replyTo.send(rMsg);
      		 } catch (RemoteException e) {
      		 }

		}
	}
	
	static class threadScan extends AsyncTask<ScanParam,String,ResultInfo> {
		Messenger replyTo;
		@Override
		protected ResultInfo doInBackground(ScanParam... params) {
			replyTo = params[0].replyTo;
			return UHFDevice.getInstance().scan(params[0].q);
		}
		
		@Override
		protected void onPreExecute() {
			
		}
		
		@Override
		protected void onPostExecute(ResultInfo result) {
			 Message rMsg = Message.obtain(null, MSG_SCAN);
      		 Bundle data = new Bundle();
      		 data.putInt("result", result.getResult());
      		 // STS_NO_TAG_FOUND = -23,
      		 if (result.getResult()==0) {
      			 //data.putString("tags", result.getValues().get(0));
      			 //List<String> tags =  result.getValues(); // duplicative
      			 ArrayList<String> tags = new ArrayList<String>();
      			 for(String s : result.getValues()) {
      				 if (!tags.contains(s))
      					 tags.add(s);
      			 }
      			 //data.putStringArrayList("tags", tags.to);
      			 //data.putStringArray("tags",tags.toArray(new String[tags.size()]));
      			 data.putStringArrayList("tags", tags);
      		 }
      		 rMsg.setData(data);
      		 try {
                replyTo.send(rMsg);
      		 } catch (RemoteException e) {
      		 }

		}
	}

	static class threadReadUser extends AsyncTask<ReadUserParam,String,ResultInfo> {
		Messenger replyTo;
		@Override
		protected ResultInfo doInBackground(ReadUserParam... params) {
			ReadUserParam prm = params[0];
			replyTo = prm.replyTo;
			return UHFDevice.getInstance().readUser(prm.blockId, prm.password, prm.readLen);
		}
		
		@Override
		protected void onPreExecute() {
			
		}
		
		@Override
		protected void onPostExecute(ResultInfo result) {
			 Message rMsg = Message.obtain(null, MSG_READUSERZONE);
      		 Bundle data = new Bundle();
      		 data.putInt("result", result.getResult());
      		 // STS_NO_TAG_FOUND = -23,
      		 if (result.getResult()==0) {
      			 String tmp = result.getResultValue();
      			 StringBuilder userzone = new StringBuilder();
      			 for (int i = 0; i < tmp.length(); i = i + 4) {
      				userzone.append(tmp.substring(i + 2, i+4));
      				userzone.append(tmp.substring(i, i+ 2));
      			 }
      			 data.putString("userzone", userzone.toString());
      		 }
      		 rMsg.setData(data);
      		 try {
                replyTo.send(rMsg);
      		 } catch (RemoteException e) {
      		 }

		}
	}

    /**
     * Target we publish for clients to send messages to IncomingHandler.
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());

	@Override
	public IBinder onBind(Intent intent) {
		//dev = UHFDevice.getInstance();
		return mMessenger.getBinder();
	}

}