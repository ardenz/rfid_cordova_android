cordova.define("com.laf148.cordova.plugin.UHFService.UHFService", function(require, exports, module) { var UHFService = {
    connect: function(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, 'UHFService', 'connect', []);
    },
    disconnect: function(successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, 'UHFService', 'disconnect', []);
    },
    echo: function(successCallback, errorCallback, msg) {
        cordova.exec(successCallback, errorCallback, 'UHFService', 'echo', [msg]);
    },
    scan: function(successCallback, errorCallback, q) {
        cordova.exec(successCallback, errorCallback, 'UHFService', 'scan', [q]);
    },
    readUser: function(successCallback, errorCallback, blockId, password, readLen) {
        cordova.exec(successCallback, errorCallback, 'UHFService', 'readUser', [blockId, password, readLen]);
    },
};
module.exports = UHFService;
});
