package com.laf148.cordova.plugin;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import java.util.ArrayList;

public class UHFService extends CordovaPlugin{
	static final int MSG_CONNECT = 1;
	static final int MSG_DISCONNECT = 2;
	static final int MSG_SCAN = 3;
	static final int MSG_READUSERZONE = 4;

	
	private static final String LOG_TAG = "UHFServicePlugin";
	
	private boolean _bounded = false;
	private Messenger mService = null;
    
	@Override
	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
	    super.initialize(cordova, webView);
        Intent intent = new Intent();
        intent.setAction("laf148.intent.action.UHFSERVICE");
        
        cordova.getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
	}

	@Override
    public void onDestroy() {
        if (_bounded) {  
        	cordova.getActivity().unbindService(mConnection);  
            _bounded = false;  
        }  
    }

   private ServiceConnection mConnection = new ServiceConnection() {  
		 
       @Override  
       public void onServiceConnected(ComponentName className,  
               IBinder service) {  
    	   mService = new Messenger(service);
           _bounded = true;  
       }  
 
       @Override  
       public void onServiceDisconnected(ComponentName arg0) {  
    	   mService = null;
    	   _bounded = false;  
       }  
   };  

	
	/**
    * Handler of incoming messages from service.
    */
   static class IncomingHandler extends Handler {
	   private CallbackContext callbackContext;
	   public IncomingHandler(CallbackContext callbackContext)
	   {
		   this.callbackContext = callbackContext;
	   }
	   
       @Override
       public void handleMessage(Message msg) {
    	   Bundle data;
    	   Integer rs;
           switch (msg.what) {
               case MSG_CONNECT:
               case MSG_DISCONNECT:
                   data = msg.getData();
                   rs = data.getInt("result");
                   if (rs==0)
                	   callbackContext.success(rs);
                   else
                	   callbackContext.error(rs);
                   break;
               case MSG_SCAN:
                   data = msg.getData();
                   rs = data.getInt("result");
                   if (rs==0) {
	   	                JSONObject obj = new JSONObject();
	   	                try {
	   	                	//JSONArray jsArray = new JSONArray(data.getStringArray("tags")); // it requires API 19
	   	                	//JSONArray ja = new JSONArray();
	   						//for (String s : data.getStringArray("tags"))
	   						//	ja.put(s);
	   						JSONArray ja = new JSONArray();
		   					 ArrayList<String> tags = new ArrayList<String>();
		   	      			 for(String s : data.getStringArrayList("tags")) {
		   	      				 if (s.length() > 0 && !tags.contains(s))
		   	      				 {
		   	      					 tags.add(s);
		   	      					 ja.put(s);
		   	      				 }
		   	      			 }
	   	                    obj.put("tags", ja);
							
	   	                } catch (JSONException e) {
	   	                    Log.d(LOG_TAG, "This should never happen");
	   	                }
                	   callbackContext.success(obj);
                   }
                   else
                	   callbackContext.error(rs);
                   break;
               case MSG_READUSERZONE:
                   data = msg.getData();
                   rs = data.getInt("result");
                   if (rs==0) {
                	   String userzone = data.getString("userzone");
                	   callbackContext.success(userzone);
                   }
                   else
                	   callbackContext.error(rs);
            	   break;
               default:
                   super.handleMessage(msg);
           }
       }
   }


    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("connect")) {
        	connect(args, callbackContext);
        } else if (action.equals("disconnect")){
        	disconnect(args, callbackContext);
        } else if (action.equals("echo")){
        	echo(args, callbackContext);
        } else if (action.equals("scan")){
        	scan(args, callbackContext);
        } else if (action.equals("readUser")){
        	readUser(args, callbackContext);
        } else {
            return false;
        }
        return true;
    }

	public void echo(JSONArray args, CallbackContext callbackContext) {
		try {
			callbackContext.success(args.getString(0));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			callbackContext.error(e.getMessage());
		}
	}
	
	public void connect(JSONArray args, CallbackContext callbackContext) {
		if (_bounded){
			try {
                    Message msg = Message.obtain(null,  MSG_CONNECT);
                    msg.replyTo = new Messenger(new IncomingHandler(callbackContext));
                    mService.send(msg);

            } catch (RemoteException e) {
            	callbackContext.error(e.getMessage());
            }

		}
		else {
        	callbackContext.error("unbinded");
		}
	}
	
	public void disconnect(JSONArray args, CallbackContext callbackContext) {
		if (_bounded){
			try {
                    Message msg = Message.obtain(null,  MSG_DISCONNECT);
                    msg.replyTo = new Messenger(new IncomingHandler(callbackContext));
                    mService.send(msg);

            } catch (RemoteException e) {
            	callbackContext.error(e.getMessage());
            }

		}
		else {
        	callbackContext.error("unbinded");
		}
	}

	public void scan(JSONArray args, CallbackContext callbackContext) {
		if (_bounded){
			try {
                    Message msg = Message.obtain(null,  MSG_SCAN);
                    
                    try {
                    	msg.arg1 = args.getInt(0);
                    }
                    catch (JSONException E)
                    {
                    	msg.arg1 = 0;
                    }
                    
                    msg.replyTo = new Messenger(new IncomingHandler(callbackContext));
                    mService.send(msg);

            } catch (RemoteException e) {
            	callbackContext.error(e.getMessage());
            }

		}
		else {
        	callbackContext.error("unbinded");
		}
	}
	
	public void readUser(JSONArray args, CallbackContext callbackContext){
		if (_bounded){
			try {
                    Message msg = Message.obtain(null,  MSG_READUSERZONE);
                    
                    try {
                    	msg.arg1 = args.getInt(0);
                    }
                    catch (JSONException E)
                    {
                    	msg.arg1 = 0;
                    }

                    try {
                    	msg.arg2 = args.getInt(2);
                    }
                    catch (JSONException E)
                    {
                    	msg.arg2 = 1;
                    }
                    
                    try {
                    	String password = args.getString(1);
                    	msg.getData().putString("password", password);
                    }
                    catch (JSONException E)
                    {
                    }

                    msg.replyTo = new Messenger(new IncomingHandler(callbackContext));
                    mService.send(msg);

            } catch (RemoteException e) {
            	callbackContext.error(e.getMessage());
            }

		}
		else {
        	callbackContext.error("unbinded");
		}
		
	}

}
