/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        app.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', app.onDeviceReady, false);
    },
	statusEle : undefined,
	resultEle : undefined,
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
		//UHFService.echo(app.fail, app.fail, 'test');
		//UHFService.connect(app.connectSuccess, app.fail);
		statusEle = document.getElementById('status');
		resultEle = document.getElementById('result1');
    },
	times : 1,
	connectSuccess : function(result)
	{
		statusEle.innerHTML = 'connected';
		//alert('Connected');
		//UHFService.disconnect(app.disconnectSuccess, app.fail);
	},
	disconnectSuccess : function(result)
	{
		statusEle.innerHTML = 'disconnected';
/*		alert('Disconnected');
		app.times++;
		if (app.times<=3){
			UHFService.connect(app.connectSuccess, app.fail);
		}
		*/
	},
	fail : function(result) {
		statusEle.innerHTML = 'fail' + result;
	},
	scanfail : function(result) {
		statusEle.innerHTML = 'fail' + result;
		app.isScanning = false;
	},
	connect : function() {
		UHFService.connect(app.connectSuccess, app.fail);
	},
	disconnect : function() {
		UHFService.connect(app.disconnectSuccess, app.fail);
	},
	isScanning : false,
	scan : function(q) {
		if (!app.isScanning)
		{
			app.isScanning = true;
			UHFService.scan(app.scanSuccess, app.scanfail, q);
		}
	},
	readUser : function() {
		UHFService.readUser(app.readUserSuccess, app.fail, 0, '', 7);
	},
	timer : undefined,
	contScan : function() {
		app.scan(4);
		app.timer = setTimeout('app.contScan()', 10);
	},
	stopScan : function() {
		clearTimeout(app.timer);
	},
	scanSuccess : function(result){
		//alert(result.tags);
		var tags = '';
		for( i in result.tags)
			tags=tags + result.tags[i] + '<br/>';
		resultEle.innerHTML = tags;
		app.isScanning = false;
	},
	readUserSuccess : function(result) {
		//alert(result);
		resultEle.innerHTML = result;
	},
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};
